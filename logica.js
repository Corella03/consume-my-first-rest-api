var request = new XMLHttpRequest();

function mostrar() { 
    request.open('GET', 'https://api.thecatapi.com/v1/images/search', true);
    request.onload = function () {
        var gatos = JSON.parse(this.response);
        gatos.forEach(gato => {
        const app = document.getElementById('root');
        const logo = document.createElement('img');
        logo.src = gato.url;
        const container = document.createElement('div');
        container.setAttribute('class', 'container');
        app.appendChild(logo);
        app.appendChild(container);
        });
    }
    request.send();
}